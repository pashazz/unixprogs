/*
  Read message 2
  figure out line count
*/
#include "type.h"
#include <string>
#include <cstdio>
#include <unistd.h>
#include <cstdlib>

int main()
{
    //Open msg queue
    key_t key = MSGKEY;
    int queue = msgget(MSGKEY, PERMS);
    _msg msg;
    msgrcv(queue, &msg, MSGSIZ, 2, 0);
    int current_start = 0;
    for (int i = 0; i < MSGSIZ; ++i)
    {
        if (msg.mtext[i] == '\0' || i+1 == MSGSIZ)
        {
            if (i+1 == MSGSIZ || msg.mtext[i+1] == '\0')
                break; //End of buffer
            std::string s(msg.mtext + current_start);
            current_start = i+1;
            pid_t pid = vfork();

            switch (pid)
            {
            case 0:
                execlp("wc", "wc", "-l", s.c_str(), (char*)NULL);
                _exit(1);
            case -1:
                perror("vfork()");
                exit(-1);
            default:
                wait(NULL);
                break;
            }

        }
    }
}
