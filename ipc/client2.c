#include <stdio.h>
#include <time.h>
#include "type.h"
#include <unistd.h>

int main()
{
    char buf[100] = { 0 };
    int queue = msgget(MSGKEY, PERMS);
    struct msqid_ds ds = { 0 };
    msgctl(queue,IPC_STAT, &ds);
    strftime(buf, sizeof(buf), "%c", localtime(&ds.msg_stime));
    printf("Last message time: %s\n", buf);
    sync();
    struct _msg msg;
    /**
       available options:
       IPC_NOWAIT - do not block if queue is full
       MSG_NOERROR - truncate data & return successfully if data > msgsz
       MSG_EXCEPT - everything except TYPE Linux only??
    */
    msgrcv(queue, &msg, MSGSIZ, 0, 0); //2nd last 0 means any message; last 0 - no options
    write(STDOUT_FILENO, &msg.mtext, MSGSIZ);
    write(STDOUT_FILENO, "\n", 1);

}
