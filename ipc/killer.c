#include <sys/msg.h>
#include "type.h"
#include <stdio.h>

int main()
{
    int queue;
    if ((queue = msgget(MSGKEY, PERMS)) == -1)
    {
        perror("msgget()");
        return 1;
    }
    if (msgctl(queue, IPC_RMID, NULL) == -1)
    {
        perror("msgctl()");
        return 2;
    }
}
