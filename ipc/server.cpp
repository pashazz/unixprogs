/*
  Implements System V queue
*/
#include "type.h"
#include <string>
#include <iostream>
#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h> //errno
#include <string.h> //strlen
#include <stdlib.h> //exit


struct fileinfo
{
    std::string fname;
    std::string type;
    std::string charset;
};

size_t append(char *dest, const char *src);
void get_dir_info(const char *directory);
fileinfo get_file_info(const char *name);

bool is_text(const fileinfo &info);
bool is_binary(const fileinfo &info);
bool is_c(const fileinfo &info);

int main()
{
    get_dir_info(".");
    return 0;
}

void get_dir_info(const char *directory)
{
    //Open this directory
    _msg msg1 = {}, msg2 = {}, msg3 = {};
    msg1.mtype = 1; //Text files
    msg2.mtype = 2; //C files
    msg3.mtype = 3; //Binary files

    DIR* dir = opendir(directory);
    if (!dir)
    {
        perror("opendir()");
        exit(-1);
    }
    errno = 0;
    dirent *entry = (dirent*)NULL;
    int pos1 = 0, pos2 = 0, pos3 = 0;
    while (entry = readdir(dir))
    {
        fileinfo inf = get_file_info(entry->d_name);
        if (is_text(inf))
            pos1 += append(msg1.mtext+pos1, entry->d_name);
        if (is_c(inf))
            pos2 += append(msg2.mtext+pos2, entry->d_name);
        if(is_binary(inf))
            pos3 += append(msg3.mtext+pos3, entry->d_name);

    }
    if (errno)
    {
        perror("readdir()");
        exit(-1);
    }
    //Create msg queue
    key_t key = MSGKEY;
    int queue = msgget(MSGKEY, IPC_CREAT | /*IPC_EXCL | */ PERMS);
    if (queue == -1)
    {
        perror("msgget()");
        exit(-1);
    }

    msqid_ds ds = {0};
    if (msgctl(queue, IPC_STAT, &ds) == -1)
    {
        perror("msgctl()");
        exit(-1);
    }

    if (MSGSIZ > ds.msg_qbytes)
    {
        fprintf(stderr, "Not enough memory in msg_qbytes: available: %d, requested %d; increasing\n", ds.msg_qbytes, MSGSIZ*MSGCOUNT);
        ds.msg_qbytes = MSGSIZ*MSGCOUNT;
        if (msgctl(queue, IPC_SET, &ds) == -1)
        {
            perror("msgctl()");
            exit(-1);
        }
        memset(&ds, 0, sizeof(msqid_ds));
        msgctl(queue, IPC_STAT, &ds);
        if (ds.msg_qbytes != MSGSIZ*MSGCOUNT)
        {
            fprintf(stderr, "Tried to increase msg_qbytes, got: %d, aborting...\n", ds.msg_qbytes);
            abort();
        }

    }
    fprintf(stderr, "queue opened: %d\n", queue);

    errno = 0;
    //The last argument is an option
    msgsnd(queue, &msg1, MSGSIZ, 0); //IPC_NOWAIT is the only aval. option
    msgsnd(queue, &msg2, MSGSIZ, 0);
    msgsnd(queue, &msg3, MSGSIZ, 0);
    if (errno)
    {
        perror("msgsnd()");
        exit(-1);
    }
    exit(0);
}



size_t append(char *dest, const char *src)
{
    if (src && strlen(src))
        return strlen(strcpy(dest, src)) + 1;
    else
        return 0;
}

fileinfo get_file_info(const char *name)
{
    int fd[2];
    pipe(fd);
    pid_t pid = fork();
    if (pid < 0)
    {
        perror("fork()");
        exit(-1);
    }
    else if (pid == 0) //child
    {
        close(STDOUT_FILENO);
        dup(fd[1]);
        close(fd[1]);
        execlp("file","file", "--mime", name, (char*)NULL);
        _exit(EXIT_FAILURE);
    }
    else
    { //parent
        close(fd[1]);
        char buf[BUFSIZ];
        ssize_t bytesRead;
        std::string out;
        while(1)
        {
            std::fill(buf, buf+BUFSIZ-1, '\0');
            int bytesRead = read(fd[0], buf, BUFSIZ);
            if(bytesRead < 1)
                break;
            out.append(buf, bytesRead);
        }
        close(fd[0]);
        //remove last \n
        fileinfo info;
        if (out.size() == 0)
        {
            std::cerr << "Warning: cannot execute 'file' with argument " << name <<std::endl;
            info.fname.assign(name);
            return info;
        }
        out.resize(out.size() - 1);
        //output is like this: filename: type; charset=something

        std::size_t index = out.find_last_of('=');
        if (index != std::string::npos &&
            out.find_last_of(':') < index) //ensure that '=' is not part of filename
        {
            info.charset = out.substr(index + 1);
            index = out.find_last_of(';');
            out.resize(index);
        }
        else
        {
            info.charset = "";
        }
        index = out.find_last_of(':');
        info.type = out.substr(index+2); /*skip colon and space */
        out.resize(index);
        info.fname = out;
        return info;
    }
}

bool is_text(const fileinfo &info)
{
    return !is_binary(info) && info.charset.size() > 0;
}

bool is_binary(const fileinfo &info)
{
    return info.charset == "binary";
}

bool is_c(const fileinfo &info)
{
    return info.type == "text/x-c";
}
