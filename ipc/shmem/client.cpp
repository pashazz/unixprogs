//
// Created by pasha on 01.12.15.
//

#include <unistd.h>
#include "type.h"
#include <sys/wait.h>

int main()
{
    //open a semaphore set (1 sem)
    int sid = semget(MSGKEY, 1, PERMS);
    if (sid == -1)
        handleError("semget()");

    //open shared memory
    int mid = shmget(MSGKEY, BLKSIZ, PERMS);
    if (mid == -1)
        handleError("shmget()");
    //wait until semaphore is not zero
    //this process adds to semzcnt
    struct sembuf operation = {0};
    operation.sem_num = 0;
    //operation.sem_flg = SEM_UNDO; //blocking, SEM_UNDO just in case if someone interrupt us
    operation.sem_flg = 0;
    operation.sem_op = 0; //wait zero
    if (semop(sid, &operation, 1) == -1)
    {
        handleError("semop()");
    }

    //attach to data
    char * data = (char*)shmat(mid, (void*)NULL,  0);
    if (data == (char*) -1)
    {
        handleError("shmat()");
    }

    //create pipe
    int fd[2];
    if (pipe(fd) == -1)
        handleError("pipe()");
    switch(fork())
    {
        case -1:
            handleError("fork()");
        case 0:
        { //child
            close(STDOUT_FILENO);
            dup2(fd[1], STDOUT_FILENO);
            close(fd[0]);
            close(fd[1]);
            execlp("who", "who", (char*)NULL);
            _exit(1);
        }
        default:
        { //parent
            if (read(fd[0], data, BLKSIZ) == -1)
                handleError("read()");
            wait(NULL);

        }

    }
    //detach from memory
    if (shmdt(data) == -1)
        handleError("shmdt()");
    //unlock semaphore
    int val = 1;
    if (semctl(sid, 0, SETVAL, val) == -1)
        handleError("semctl()");
    return 0;
}