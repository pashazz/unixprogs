#include <stdio.h>
#include <cstdlib>
#include <ctime>
#include "type.h"
#include <unistd.h>
#include <signal.h>
#include <sstream>
#include <iostream>


#define INTERRUPT "Interrupt...\n"
int sid, mid;
void parse_data(char * data);
void atExit()
{
    //Remove shared memory
    if (shmctl(mid, IPC_RMID, NULL) == -1)
        handleError("shmctl()");
    //Remove semaphore
    if(semctl(sid, 0, IPC_RMID) == -1)
        handleError("semctl()");
}
void interrupt_handler(int signum)
{

    write(STDOUT_FILENO, INTERRUPT, sizeof(INTERRUPT));
    exit(1);

}

int main()
{
    atexit(atExit);
    //Create a semaphore set of 1 semaphore (exclusive)
    sid = semget(MSGKEY, 1, IPC_CREAT | IPC_EXCL | PERMS);
    if (sid == -1)
        handleError("semget()");

    //Initialize  0th semaphore to 0
    int val  = 0;
    if (semctl(sid, 0, SETVAL, val) == -1)
        handleError("semctl()");

    //create shmem block
    mid = shmget(MSGKEY, BLKSIZ, IPC_CREAT | IPC_EXCL | PERMS);
    if (mid == -1)
        handleError("shmget()");
    fprintf(stderr, "Memory block allocated: size %d, id %d\n", BLKSIZ, mid);
    signal(SIGINT, interrupt_handler);
    //Block until client arrives with the data
    struct sembuf operation = {0};
    operation.sem_num = 0; //0th semaphore
    /*
      -1 means: wait until semval will be greater or equal than 1.
      this process is added to semncnt.
      semadj is +1  so that it can -1 when process ends
      1 will be substracted from semval after unblocking
    */
    operation.sem_op = -1;
    operation.sem_flg = 0; //No undo necessary, and blocking.
    if (semop(sid, &operation, 1) /* <- 1 semafore available */ == -1)
        handleError("semop()");


    //read data from shared memory
    //attach as read only (no preference where to put)
    char * data = (char*)shmat(mid, (void*)NULL,  SHM_RDONLY);
    if (data == (char*)-1)
        handleError("shmat()");

    parse_data(data);
    //Detach me from memory
    if (shmdt(data) == -1)
        handleError("shmdt()");

    //Print semaphore ctime

    struct semid_ds seminfo = {0};
    if (semctl(sid, 0, IPC_STAT, &seminfo) == -1)
        handleError("semctl()");

    char buf[100] = {0};
    strftime(buf, sizeof(buf), "%c", localtime(&seminfo.sem_ctime));
    printf("Last modification time: %s\n", buf);
    return 0;

}

void parse_data(char *data)
{
    //parse \ns
    std::istringstream stream ((std::string(data)));
    std::string line;
    while (std::getline(stream, line))
    {
        std::istringstream linestream(line);
        std::string user;
        linestream >> user;
        std::string ignore;
        linestream >> ignore; //tty
        std::string date;
        linestream >> date;
        std::string sTime;
        linestream >> sTime;
        tm t = {};
        strptime(date.c_str(), "%Y-%m-%d", &t); //2015-11-12
        strptime(sTime.c_str(), "%R", &t);//00:00
        char buf[100];
        strftime(buf, sizeof(buf), "%c", &t);
        std::cerr << "time for user " << user << ": "<< buf << std::endl;
        double dTime = difftime(time(NULL), mktime(&t)) / 60;
        if (dTime > 20)
        {
            std::cerr << user << ": " << int(dTime) << " minutes" << std::endl;
        }
    }
}
