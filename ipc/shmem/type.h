#include <sys/sem.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>

#define MSGKEY ftok("server",1)
#define PERMS 0666
#define BLKSIZ 8192

void handleError(const char * msg)
{
    perror(msg);
    exit(1);
}
