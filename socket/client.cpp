#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <iostream>

#define FILENAME "/tmp/pssocket.socket"
#define NOCONTROLLINGTERMINAL 1 //only print processes without controlling terminal
#define CONTROLLINGTERMINAL 2 // only print processes with controlling terminal
void print_controlling_terminal();
void print_no_ctrl_terminal();
int main(int argc, char** argv)
{
    if (argc < 2)
    {
        fprintf(stderr, "Too few arguments \n");
        return -1;
    }
    int sock;
    struct sockaddr_un saddr;

    //SOCKET(2) 3rd argument is protocol. See /etc/protocols
    if ((sock = socket(PF_UNIX,SOCK_STREAM, 0)) == -1)
    {
        perror("socket()");
        return EXIT_FAILURE;
    }
    //STRTOL(3) 2nd argument is pointer which will store first invalid character
    int code = strtol(argv[1], NULL, 10);
    if (code != 1 && code != 2)
    {
        fprintf(stderr, "Invalid client request\n");
        return -1;
    }
    saddr.sun_family = AF_UNIX;
    strcpy(saddr.sun_path, FILENAME);
    //SUN_LEN returns address size
    if (connect(sock, (struct sockaddr *) &saddr, SUN_LEN(&saddr)) == -1)
    {
        perror("connect()");
        return EXIT_FAILURE;
    }

    //Redirect stdout to socket
    dup2(sock, STDOUT_FILENO);
    //    dup2(sock, STDERR_FILENO);
    close(sock);
    if (code == NOCONTROLLINGTERMINAL)
        print_no_ctrl_terminal();
    else
        print_controlling_terminal();
    return -1; //Not reaching that
}

void print_no_ctrl_terminal()
{ //ps -e -o tty,pid,command and then filter data written by this command by leading ?. Then exit
    int fd[2];
    pipe(fd);
    pid_t pid = fork();
    if (pid < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else if (pid == 0) //child
    {
        close(fd[0]);
        close(STDOUT_FILENO);
        dup2(fd[1], STDOUT_FILENO);
        close(fd[1]);
        char* pscmd[5];
        pscmd[0] = "ps";
        pscmd[1] = "ax";
        pscmd[2] = "-o";
        pscmd[3] = "tty,stat,pid,comm";
        pscmd[4] = (char*)NULL;

        execvp("ps", pscmd);
        _exit(0);
    }
    else if (pid > 0) //parent
    {
        close(fd[1]);
        //Read from file descriptor line by line
        std::string str;
        int read_current_line = 1;
        //List of possible read_current_line values:
        //0:Line skipped
        //-1:Need to decide if we should skip the line
        //1:Line started, first space symbol is  not reached
        //2:Line started, first space symbol is reached;
        //3: Line started, last space symbol in group is reached, copy everything else
        char buf[BUFSIZ];
        while(1)
        {

            std::fill(buf, buf+BUFSIZ-1, '\0');
            const ssize_t bytesRead = read(fd[0], buf, BUFSIZ);
            if (bytesRead < 1) break;

            for (ssize_t i; i < bytesRead; ++i)
            {
                if (read_current_line == -1)
                    read_current_line = (buf[i] == '?' ? 1 : 0);
                if(buf[i] == '\n')
                {
                    if (read_current_line)
                    {
                        std::cout << str << std::endl;
                        str.clear();
                    }
                    read_current_line = -1;
                }
                else
                {
                    if (read_current_line == 1 && isspace(buf[i]))
                        read_current_line = 2;
                    else if (read_current_line == 2 && !isspace(buf[i]))
                        read_current_line = 3;
                    if (read_current_line == 3)
                        str += buf[i];

                }
            }
        }
        exit(0);
    }
}

void print_controlling_terminal()
{//ps a -o stat,pid,comm
    execlp("ps","ps", "a", "-o", "stat,pid,comm", (char*)NULL);
    _exit(EXIT_FAILURE);
}
