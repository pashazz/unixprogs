#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include <signal.h>

#define FILENAME "/tmp/pssocket.socket"
void print(const std::string &str);
int sock = 0 ,cl_sock = 0;

void sighandler(int signum)
{ //terminate normally :)

    std::cout << "process terminated" << std::endl;
    exit(0);
}
void close_sock()
{ //remove sockets so that I don't need to remove it myself
    if(sock)
        close(sock);
    if (cl_sock)
        close(cl_sock);
    unlink(FILENAME);
}
int main()
{
    atexit(close_sock);
    signal(SIGINT, sighandler);
    struct sockaddr_un saddr;
    char buf[BUFSIZ + 1];
    if ((sock = socket(PF_UNIX,SOCK_STREAM, 0)) == -1) //Unix connection domain , Streamed socket type (requires connection), protocol 0
        //protocol 0 is Default, see /etc/protocols for more
        //For SOCK_STREAM, TCP is default
        //For SOCK_DGRAM, UDP is default
        //STREAM- two-way reliable connection based byte stream
        //DGRAM - connectionless unreliable messge of fixed length
    {
        perror("socket()");
        return EXIT_FAILURE;
    }
    saddr.sun_family = AF_UNIX;
    strcpy(saddr.sun_path, FILENAME);
    if (bind(sock, (struct sockaddr*)&saddr, SUN_LEN(&saddr)) == -1)
        //SUN_LEN returns address length
    {
        perror("bind()");
        return EXIT_FAILURE;
    }
    if (listen(sock, SOMAXCONN) == -1)
    {
        perror("listen()");
        return EXIT_FAILURE;
    }


    while(1)
    {
        //ACCEPT(2)
        //Accept connection, 2nd argument is filled with the sockaddr structure of the client, 3rd - address len.
        cl_sock = accept(sock, NULL, NULL);

        //Receive data
        std::string str;
        while(1)
        {
            //last argument is options.
            //Might be one of
            //MSG_OOB process out-of-band-data
            //MSG_PEEK       peek at incoming message
            //MSG_WAITALL    wait for full request or error

            ssize_t nread = recv(cl_sock, buf, BUFSIZ, 0);
            if (nread > 0)
            {
                for(size_t i = 0; i < nread; i++)
                {
                    if(buf[i] == '\n')
                    {
                        print(str);
                        str.clear();
                    }
                    else
                        str += buf[i];
                }
            }
            else
            {
                //              fprintfc(stderr, "Connection closed\n");
                break;
            }
        }
        close(cl_sock);

    }
    return EXIT_SUCCESS;
}


void print(const std::string &str)
{
    if(str[0] == 'S')
    {
        std::cout << str << std::endl;
    }
}
